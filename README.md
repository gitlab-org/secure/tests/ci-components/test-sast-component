# Test SAST CI Component

Serves as a demo project to verify SAST [CI Component](https://gitlab.com/gitlab-components/sast) is working in line with the equivelant [CI template](https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/lib/gitlab/ci/templates/Jobs/SAST.gitlab-ci.yml).
